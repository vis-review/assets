<?php

namespace Cdl\Vis\Review;

interface VehicleReviewInterface
{

    /**
     * @return string
     */
    public function getVehicleType();

    /**
     * @return string
     */
    public function getVehicleMake();

    /**
     * @return string
     */
    public function getVehicleModel();

    /**
     * @return int
     */
    public function getVehicleYearOfManufacture();

    /**
     * @return int
     */
    public function getVehicleYearofDiscontinuation();

    /**
     * @return string
     */
    public function getReview();
}
